Release instructions
====================

1. Tag the desired commit
2. Push tag to official repository
3. Run build via make (`make clean networking-generic-switch-fastpath.deb`)
4. Upload networking-generic-switch-fastpath.deb to Bitbucket under the Downloads section.
5. Copy networking-generic-switch-fastpath.deb to networking-generic-switch-fastpath-$VERSION.deb and 
   upload it to Bitbucket, under the Downloads section. This ensures users can point to specific
   versions of the package.
