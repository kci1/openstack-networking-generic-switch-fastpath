# Copyright 2017 Kontron Canada Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from netman.adapters.shell.ssh import SshClient
from netman.adapters.switches.util import SubShell, ResultChecker
from netman.core.objects.exceptions import UnknownVlan
from netman.core.objects.switch_base import SwitchBase


def ssh(switch_descriptor):
    return Pica8Switch(switch_descriptor, shell_factory=SshClient)


# Minimalist Pica8 CLI switch implementation.
# Allows basic implementation for custom NGS netman driver.
class Pica8Switch(SwitchBase):
    def __init__(self, switch_descriptor, shell_factory):
        super(Pica8Switch, self).__init__(switch_descriptor)
        self.shell = None
        self.shell_factory = shell_factory

    def _connect(self):
        params = dict(
            host=self.switch_descriptor.hostname,
            username=self.switch_descriptor.username,
            password=self.switch_descriptor.password,
            prompt=('$ ', '# ', '> ')
        )

        if self.switch_descriptor.port:
            params["port"] = int(self.switch_descriptor.port)

        self.shell = self.shell_factory(**params)
        self.shell.do("cli")

    def _disconnect(self):
        self.shell.do("exit")
        self.shell.quit("exit")
        self.logger.info(self.shell.full_log)

    def add_vlan(self, number, name=None):
        with self.config():
            self.shell.do("set vlans vlan-id {vlan_id} vlan-name {vlan_name}".format(vlan_id=number, vlan_name=name))
            self.shell.do("commit")

    def remove_vlan(self, number):
        with self.config():
            self.shell.do("delete vlans vlan-id {vlan_id}".format(vlan_id=number))
            self.shell.do("commit")

    def set_access_vlan(self, interface_id, vlan):
        # Using native-vlan-id mimics using pvid on FASTPATH.
        with self.config():
            self.set("set interface gigabit-ethernet {} family ethernet-switching native-vlan-id {}", interface_id, vlan)
            self.set("delete interface gigabit-ethernet {} family ethernet-switching vlan members {}", interface_id, vlan)
            self.set("commit")\
                .on_result_matching(".*Vlan \d+ does not exist.*", UnknownVlan, vlan)


    def unset_interface_access_vlan_specific_vlan(self, interface_id, vlan):
        with self.config():
            self.set("set interface gigabit-ethernet {} family ethernet-switching native-vlan-id {}", interface_id, 1)
            self.set("commit")

    def config(self):
        return SubShell(self.shell, enter='configure', exit_cmd='exit discard')

    def set(self, command, *arguments):
        result = self.shell.do(command.format(*arguments))
        return ResultChecker(result)
