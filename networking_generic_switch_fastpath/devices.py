# Copyright 2017 Kontron Canada Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import atexit

from netman.core.objects.switch_descriptor import SwitchDescriptor
from networking_generic_switch import locking
import networking_generic_switch.config
from networking_generic_switch.devices import GenericSwitchDevice
from contextlib import contextmanager

import tooz.coordination

from networking_generic_switch_fastpath import fastpath_switch, pica8_switch


@contextmanager
def connecting_context(switch):
    switch.connect()
    yield
    switch.disconnect()


CONF = networking_generic_switch.config.CONF


class NetmanBasedSwitch(GenericSwitchDevice):
    netman_switch_model = None

    def __init__(self, device_cfg):
        super(NetmanBasedSwitch, self).__init__(device_cfg)

        descriptor = SwitchDescriptor(
            model=self.netman_switch_model,
            hostname=device_cfg.get('ip'),
            port=device_cfg.get('port'),
            username=device_cfg.get('username'),
            password=device_cfg.get('password'),
        )
        self.switch = self.factory(descriptor)

        self.locker = tooz.coordination.get_coordinator(self.get_backend_url(), ('ngs-' + CONF.host).encode('ascii'))
        self.locker.start()
        atexit.register(self.locker.stop)

        self.lock_kwargs = {
            'locks_pool_size': 1,
            'locks_prefix': device_cfg.get('ip'),
            'timeout': CONF.ngs_coordination.acquire_timeout
        }

    def factory(self, descriptor):
        pass

    def get_vlan_name(self, network_id):
        return network_id.replace('-', '')

    def get_backend_url(self):
        return CONF.ngs_coordination.backend_url if CONF.ngs_coordination.backend_url else CONF.database.connection

    def add_network(self, segmentation_id, network_id):
        with locking.PoolLock(self.locker, **self.lock_kwargs):
            with connecting_context(self.switch):
                self.switch.add_vlan(segmentation_id, self.get_vlan_name(network_id))

    def del_network(self, segmentation_id, **kwargs):
        return

    def plug_port_to_network(self, port, segmentation_id):
        with locking.PoolLock(self.locker, **self.lock_kwargs):
            with connecting_context(self.switch):
                self.switch.set_access_vlan(port, segmentation_id)

    def delete_port(self, port, segmentation_id):
        with locking.PoolLock(self.locker, **self.lock_kwargs):
            with connecting_context(self.switch):
                self.switch.unset_interface_access_vlan_specific_vlan(port, segmentation_id)


class NetmanPica8Switch(NetmanBasedSwitch):
    netman_switch_model = 'pica8'

    def factory(self, descriptor):
        return pica8_switch.ssh(descriptor)


class NetmanFastpathSwitch(NetmanBasedSwitch):
    netman_switch_model = 'fastpath'

    def factory(self, descriptor):
        return fastpath_switch.telnet(descriptor)
