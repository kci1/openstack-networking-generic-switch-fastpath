# Copyright 2017 Kontron Canada Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from netman import regex
from netman.adapters.shell.telnet import TelnetClient
from netman.adapters.switches.util import SubShell, no_output, ResultChecker, PageReader
from netman.core.objects.exceptions import UnknownInterface, BadVlanName, \
    BadVlanNumber, UnknownVlan, VlanAlreadyExist
from netman.core.objects.switch_base import SwitchBase
from netman.core.objects.vlan import Vlan


def telnet(switch_descriptor):
    return FastpathSwitch(switch_descriptor, shell_factory=TelnetClient)


class FastpathSwitch(SwitchBase):

    def __init__(self, switch_descriptor, shell_factory):
        super(FastpathSwitch, self).__init__(switch_descriptor)
        self.shell = None
        self.shell_factory = shell_factory

        self.page_reader = PageReader(
            read_while="--More-- or (q)uit",
            and_press="m",
            unless_prompt="#"
        )

    def _connect(self):
        params = dict(
            host=self.switch_descriptor.hostname,
            username=self.switch_descriptor.username,
            password=self.switch_descriptor.password,
        )

        if self.switch_descriptor.port:
            params["port"] = int(self.switch_descriptor.port)

        self.shell = self.shell_factory(**params)
        self.shell.do("enable")

    def _disconnect(self):
        self.shell.do("exit")
        self.shell.quit("quit")
        self.logger.info(self.shell.full_log)

    def _start_transaction(self):
        pass

    def _end_transaction(self):
        pass

    def rollback_transaction(self):
        pass

    def commit_transaction(self):
        self.logger.warn('Commit operation discarded.')

    def get_vlans(self):
        result = self.page_reader.do(self.shell, "show vlan")
        vlans = parse_vlan_list(result)
        return vlans

    def add_vlan(self, number, name=None):
        result = self.page_reader.do(self.shell, "show vlan {}".format(number))
        if len(result) == 2 and regex.match(".*1 to 4093\.", result[1]):
            raise BadVlanNumber()
        elif regex.match("^VLAN ID\:", result[0]):
            raise VlanAlreadyExist(number)

        with self.vlan_database():
            self.set('vlan {}', number)

            if name is not None:
                self.set('vlan name {} {}'.format(number, name)).on_any_result(BadVlanName)

    def remove_vlan(self, number, name=None):
        with self.vlan_database():
            self.set('no vlan {}', number).on_result_matching("^Failed to delete one or more VLAN.*", UnknownVlan, number)

    def set_access_mode(self, interface_id):
        pass

    def set_access_vlan(self, interface_id, vlan):
        with self.config(), self.interface(interface_id):
            self.set("vlan pvid {}", vlan) \
                .on_result_matching(".*Couldn't configure pvid\.$", UnknownVlan, vlan)
            self.set("vlan participation include {}", vlan)
            self.set("no vlan tagging {}", vlan)

    def unset_interface_access_vlan_specific_vlan(self, interface_id, vlan):
        with self.config(), self.interface(interface_id):
            self.set("no vlan pvid {}", vlan) \
                .on_result_matching(".*\% Invalid input detected.*", UnknownVlan, vlan)
            self.set("vlan participation auto {}", vlan)

    def config(self):
        return SubShell(self.shell, enter="configure", exit_cmd='exit')

    def vlan_database(self):
        return SubShell(self.shell, enter="vlan database", exit_cmd='exit')

    def interface(self, interface_id):
        return SubShell(self.shell, enter="interface {}".format(interface_id), exit_cmd='exit',
                        validate=no_output(UnknownInterface, interface_id))

    def set(self, command, *arguments):
        result = self.shell.do(command.format(*arguments))

        return ResultChecker(result)


def parse_vlan_list(result):
    vlans = []
    for line in result:
        if regex.match('^(\d+)(.*)', line):
            number, leftovers = regex
            name = None
            if regex.match('^\s{1,6}(\S+).*', leftovers):
                name = regex[0]

            if name == ("VLAN{}".format(number)):
                name = None

            vlan = Vlan(number=int(number),
                        name=name if int(number) > 1 else "default")
            vlans.append(vlan)
    return vlans
