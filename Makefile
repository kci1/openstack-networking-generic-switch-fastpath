.PHONY: all clean

all: clean networking-generic-switch-fastpath.deb

clean:
	rm -rf dist/
	rm -f *.deb

networking-generic-switch-fastpath.deb:
	python setup.py sdist
	docker build -t $@ .
	docker run $@ bash -c 'cat /root/*.deb' \
		> $@
