FROM ubuntu:16.04

RUN apt-get update && \
    apt-get install -y software-properties-common && \
    add-apt-repository cloud-archive:newton
RUN apt-get update && \
    apt-get install -y checkinstall python-pip git python-neutron \
                       python-dev libxml2-dev libxslt1-dev zlib1g-dev
COPY portbindings.patch /root/portbindings.patch
COPY dist /root/dist
RUN tar --wildcards --extract -O -f /root/dist/openstack-networking-generic-switch-fastpath-*.tar.gz openstack-networking-generic-switch-fastpath-*/PKG-INFO | grep '^Version' | head -n1 | cut -f2 -d' ' > /root/version.txt
RUN cd /root && checkinstall \
                 --pkgname python-openstack-networking-generic-switch-fastpath \
                 --pkgversion $(cat /root/version.txt) \
                 --default --exclude /tmp --exclude /var --exclude /root -D \
                 /bin/bash -c " \
                 pip install \
                 --global-option="--no-user-cfg" \
                 --install-option="--prefix=/usr" \
                 --install-option="--no-compile" \
                 $(pip freeze) \
                 dist/openstack-networking-generic-switch-fastpath*.tar.gz \
                 && cp -a /usr/lib/python2.7/site-packages/* /usr/lib/python2.7/dist-packages/ \
                 && patch -p1 -d /usr/lib/python2.7/dist-packages/ < /root/portbindings.patch \
                 && rm -rf /usr/lib/python2.7/site-packages \
                 && rm -rf /usr/lib/python2.7/dist-packages/msgpack*"
