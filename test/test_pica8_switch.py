# Copyright 2015 Internap.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import unittest

import mock
from flexmock import flexmock, flexmock_teardown
from hamcrest import assert_that, equal_to, is_
from netman.adapters.switches.util import SubShell
from netman.core.objects.exceptions import UnknownVlan
from netman.core.objects.switch_descriptor import SwitchDescriptor

from networking_generic_switch_fastpath import pica8_switch


class Pica8Test(unittest.TestCase):
    def setUp(self):
        self.switch = pica8_switch.Pica8Switch(
            SwitchDescriptor(model='pica8', hostname="my.hostname", password="the_password"), None)
        SubShell.debug = True
        self.mocked_ssh_client = flexmock()
        self.switch.shell = self.mocked_ssh_client

    def tearDown(self):
        flexmock_teardown()

    def test_switch_has_a_logger_configured_with_the_switch_name(self):
        assert_that(self.switch.logger.name, is_(pica8_switch.Pica8Switch.__module__ + ".my.hostname"))

    @mock.patch("netman.adapters.shell.ssh.SshClient")
    def test_connect_enters_cli_mode(self, ssh_client_class_mock):
        self.switch = pica8_switch.Pica8Switch(
            SwitchDescriptor(hostname="my.hostname", username="the_user", password="the_password", model="fastpath"),
            shell_factory=ssh_client_class_mock)

        self.mocked_ssh_client = flexmock()
        ssh_client_class_mock.return_value = self.mocked_ssh_client
        self.mocked_ssh_client.should_receive("do").with_args("cli").once().and_return([])

        self.switch.connect()

        ssh_client_class_mock.assert_called_with(
            host="my.hostname",
            username="the_user",
            password="the_password",
            prompt=('$ ', '# ', '> ')
        )

    @mock.patch("netman.adapters.shell.ssh.SshClient")
    def test_add_vlan(self, ssh_client_class_mock):
        self.switch = pica8_switch.Pica8Switch(
            SwitchDescriptor(hostname="my.hostname", username="the_user", password="the_password", model="fastpath"),
            shell_factory=ssh_client_class_mock)

        self.mocked_ssh_client = flexmock()
        ssh_client_class_mock.return_value = self.mocked_ssh_client
        self.mocked_ssh_client.should_receive("do").with_args("cli").once().and_return([])
        self.mocked_ssh_client.should_receive("do").with_args("configure").once().and_return([])
        self.mocked_ssh_client.should_receive("do").with_args("set vlans vlan-id 1234 vlan-name HELLO").once().and_return([])
        self.mocked_ssh_client.should_receive("do").with_args("commit").once().and_return([])
        self.mocked_ssh_client.should_receive("do").with_args("exit discard").once().and_return([])

        self.switch.connect()
        self.switch.add_vlan(1234, "HELLO")

    @mock.patch("netman.adapters.shell.ssh.SshClient")
    def test_remove_vlan(self, ssh_client_class_mock):
        self.switch = pica8_switch.Pica8Switch(
            SwitchDescriptor(hostname="my.hostname", username="the_user", password="the_password", model="fastpath"),
            shell_factory=ssh_client_class_mock)

        self.mocked_ssh_client = flexmock()
        ssh_client_class_mock.return_value = self.mocked_ssh_client
        self.mocked_ssh_client.should_receive("do").with_args("cli").once().and_return([])
        self.mocked_ssh_client.should_receive("do").with_args("configure").once().and_return([])
        self.mocked_ssh_client.should_receive("do").with_args("delete vlans vlan-id 1234").once().and_return([])
        self.mocked_ssh_client.should_receive("do").with_args("commit").once().and_return([])
        self.mocked_ssh_client.should_receive("do").with_args("exit discard").once().and_return([])

        self.switch.connect()
        self.switch.remove_vlan(1234)

    @mock.patch("netman.adapters.shell.ssh.SshClient")
    def test_set_access_vlan(self, ssh_client_class_mock):
        self.switch = pica8_switch.Pica8Switch(
            SwitchDescriptor(hostname="my.hostname", username="the_user", password="the_password", model="fastpath"),
            shell_factory=ssh_client_class_mock)

        self.mocked_ssh_client = flexmock()
        ssh_client_class_mock.return_value = self.mocked_ssh_client
        self.mocked_ssh_client.should_receive("do").with_args("cli").once().and_return([])
        self.mocked_ssh_client.should_receive("do").with_args("configure").once().and_return([])
        self.mocked_ssh_client.should_receive("do").with_args("set interface gigabit-ethernet xe-1/1/48 family ethernet-switching native-vlan-id 1234").once().and_return([])
        self.mocked_ssh_client.should_receive("do").with_args("delete interface gigabit-ethernet xe-1/1/48 family ethernet-switching vlan members 1234").once().and_return([])
        self.mocked_ssh_client.should_receive("do").with_args("commit").once().and_return([])
        self.mocked_ssh_client.should_receive("do").with_args("exit discard").once().and_return([])

        self.switch.connect()
        self.switch.set_access_vlan("xe-1/1/48", 1234)

    @mock.patch("netman.adapters.shell.ssh.SshClient")
    def test_set_access_vlan_vlan_not_set_as_trunked(self, ssh_client_class_mock):
        self.switch = pica8_switch.Pica8Switch(
            SwitchDescriptor(hostname="my.hostname", username="the_user", password="the_password", model="fastpath"),
            shell_factory=ssh_client_class_mock)

        self.mocked_ssh_client = flexmock()
        ssh_client_class_mock.return_value = self.mocked_ssh_client
        self.mocked_ssh_client.should_receive("do").with_args("cli").once().and_return([])
        self.mocked_ssh_client.should_receive("do").with_args("configure").once().and_return([])
        self.mocked_ssh_client.should_receive("do").with_args("set interface gigabit-ethernet xe-1/1/48 family ethernet-switching native-vlan-id 1234").once().and_return([])
        self.mocked_ssh_client.should_receive("do").with_args("delete interface gigabit-ethernet xe-1/1/48 family ethernet-switching vlan members 1234").once().and_return([
            "                                                                                                  ^",
            "syntax error, expecting '4092', '4093', '10', '20', or '30'."
        ])
        self.mocked_ssh_client.should_receive("do").with_args("commit").once().and_return([])
        self.mocked_ssh_client.should_receive("do").with_args("exit discard").once().and_return([])

        self.switch.connect()
        self.switch.set_access_vlan("xe-1/1/48", 1234)

    @mock.patch("netman.adapters.shell.ssh.SshClient")
    def test_set_access_vlan_vlan_does_not_exist(self, ssh_client_class_mock):
        self.switch = pica8_switch.Pica8Switch(
            SwitchDescriptor(hostname="my.hostname", username="the_user", password="the_password", model="fastpath"),
            shell_factory=ssh_client_class_mock)

        self.mocked_ssh_client = flexmock()
        ssh_client_class_mock.return_value = self.mocked_ssh_client
        self.mocked_ssh_client.should_receive("do").with_args("cli").once().and_return([])
        self.mocked_ssh_client.should_receive("do").with_args("configure").once().and_return([])
        self.mocked_ssh_client.should_receive("do").with_args("set interface gigabit-ethernet xe-1/1/48 family ethernet-switching native-vlan-id 1234").once().and_return([])
        self.mocked_ssh_client.should_receive("do").with_args("delete interface gigabit-ethernet xe-1/1/48 family ethernet-switching vlan members 1234").once().and_return([])
        self.mocked_ssh_client.should_receive("do").with_args("commit").once().and_return([
            "admin@Switch1# commit",
            "Vlan 1234 does not exist",
            "Commit failed."
        ])
        self.mocked_ssh_client.should_receive("do").with_args("exit discard").once().and_return([])

        self.switch.connect()
        with self.assertRaises(UnknownVlan) as expect:
            self.switch.set_access_vlan("xe-1/1/48", 1234)

        assert_that(str(expect.exception), equal_to("Vlan 1234 not found"))

    @mock.patch("netman.adapters.shell.ssh.SshClient")
    def test_unset_interface_access_vlan_specific_vlan(self, ssh_client_class_mock):
        self.switch = pica8_switch.Pica8Switch(
            SwitchDescriptor(hostname="my.hostname", username="the_user", password="the_password", model="fastpath"),
            shell_factory=ssh_client_class_mock)

        self.mocked_ssh_client = flexmock()
        ssh_client_class_mock.return_value = self.mocked_ssh_client
        self.mocked_ssh_client.should_receive("do").with_args("cli").once().and_return([])
        self.mocked_ssh_client.should_receive("do").with_args("configure").once().and_return([])
        self.mocked_ssh_client.should_receive("do").with_args("set interface gigabit-ethernet xe-1/1/48 family ethernet-switching native-vlan-id 1").once().and_return([])
        self.mocked_ssh_client.should_receive("do").with_args("commit").once().and_return([])
        self.mocked_ssh_client.should_receive("do").with_args("exit discard").once().and_return([])

        self.switch.connect()
        self.switch.unset_interface_access_vlan_specific_vlan("xe-1/1/48", 1234)

    def test_disconnect(self):
        logger = flexmock()
        self.switch.logger = logger
        logger.should_receive("debug")

        mocked_ssh_client = flexmock()
        self.switch.shell = mocked_ssh_client
        mocked_ssh_client.should_receive("do").with_args("exit").once().ordered()
        mocked_ssh_client.should_receive("quit").with_args("exit").once().ordered()

        logger.should_receive("info").with_args("FULL TRANSACTION LOG").once()

        self.switch.shell.full_log = "FULL TRANSACTION LOG"

        self.switch.disconnect()
