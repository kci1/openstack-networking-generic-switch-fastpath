# Copyright 2017 Kontron Canada Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from unittest import TestCase

from mock import mock

from networking_generic_switch_fastpath.devices import NetmanFastpathSwitch


class TestNetmanGenericSwitchDevice(TestCase):

    @mock.patch('networking_generic_switch_fastpath.devices.CONF')
    def setUp(self, conf):
        self.conf = conf
        self.switch_mock = mock.Mock()

        NetmanFastpathSwitch.factory = lambda _, descriptor: self.switch_mock

        config = {
            'ip': '127.0.0.1',
            'username': 'user',
            'password': 'pass'
        }
        self.mock_coordinator = mock.Mock()

        with mock.patch('tooz.coordination.get_coordinator') as mock_get_coordinator:
            mock_get_coordinator.return_value = self.mock_coordinator
            self.switch = NetmanFastpathSwitch(config)

    def test_plug_port_to_network(self):
        self.switch.plug_port_to_network(segmentation_id=123, port='fe-1/2/3')

        self.switch_mock.assert_has_calls([
            mock.call.set_access_vlan('fe-1/2/3', 123)
        ])

    def test_delete_port(self):
        self.switch.delete_port(port='fe-1/2/3', segmentation_id=123)

        self.switch_mock.assert_has_calls([
            mock.call.unset_interface_access_vlan_specific_vlan('fe-1/2/3', 123)
        ])

    def test_add_network(self):
        self.switch.add_network(segmentation_id=123, network_id='aaaa-bbbb-1234')

        self.switch_mock.assert_has_calls([
            mock.call.add_vlan(123, 'aaaabbbb1234'),
        ])

    def test_del_network(self):
        self.switch.del_network(segmentation_id=123)

        self.switch_mock.remove_vlan.assert_not_called()
