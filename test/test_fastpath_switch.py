# Copyright 2015 Internap.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import unittest
from contextlib import contextmanager

import mock
from flexmock import flexmock, flexmock_teardown
from hamcrest import assert_that, equal_to, is_, has_length
from netman.adapters.switches.util import SubShell
from netman.core.objects.exceptions import UnknownInterface, BadVlanNumber, \
    BadVlanName, UnknownVlan, VlanAlreadyExist
from netman.core.objects.switch_descriptor import SwitchDescriptor
from networking_generic_switch_fastpath import fastpath_switch


class FastpathTest(unittest.TestCase):
    def setUp(self):
        self.switch = fastpath_switch.FastpathSwitch(SwitchDescriptor(model='fastpath', hostname="my.hostname", password="the_password"), None)
        SubShell.debug = True
        self.mocked_ssh_client = flexmock()
        self.switch.shell = self.mocked_ssh_client

    def tearDown(self):
        flexmock_teardown()

    def test_switch_has_a_logger_configured_with_the_switch_name(self):
        assert_that(self.switch.logger.name, is_(fastpath_switch.FastpathSwitch.__module__ + ".my.hostname"))

    @mock.patch("netman.adapters.shell.telnet.TelnetClient")
    def test_connect_without_port_uses_default(self, telnet_client_class_mock):
        self.switch = fastpath_switch.FastpathSwitch(
            SwitchDescriptor(hostname="my.hostname", username="the_user", password="the_password", model="fastpath"),
            shell_factory=telnet_client_class_mock)

        self.mocked_ssh_client = flexmock()
        telnet_client_class_mock.return_value = self.mocked_ssh_client
        self.mocked_ssh_client.should_receive("do").with_args("enable").and_return([])

        self.switch.connect()

        telnet_client_class_mock.assert_called_with(
            host="my.hostname",
            username="the_user",
            password="the_password"
        )

    def test_disconnect(self):
        logger = flexmock()
        self.switch.logger = logger
        logger.should_receive("debug")

        mocked_ssh_client = flexmock()
        self.switch.shell = mocked_ssh_client
        mocked_ssh_client.should_receive("do").with_args("exit").once().ordered()
        mocked_ssh_client.should_receive("quit").with_args("quit").once().ordered()

        logger.should_receive("info").with_args("FULL TRANSACTION LOG").once()

        self.switch.shell.full_log = "FULL TRANSACTION LOG"
        self.switch.disconnect()

    def test_get_vlans(self):
        flexmock(self.switch.page_reader).should_receive("do").with_args(self.mocked_ssh_client, "show vlan").once().ordered().and_return([

            "VLAN ID VLAN Name                        VLAN Type",
            "------- -------------------------------- -------------------",
            "1       default                          Default",
            "300     VLAN300                          Static",
            "2000    meh                              Static",
            "4000    VLAN4000                         Static"
        ])

        vlan1, vlan300, vlan2000, vlan4000 = self.switch.get_vlans()

        assert_that(vlan1.number, equal_to(1))
        assert_that(vlan1.name, equal_to("default"))
        assert_that(vlan1.ips, has_length(0))

        assert_that(vlan300.number, equal_to(300))
        assert_that(vlan300.name, equal_to(None))
        assert_that(len(vlan300.ips), equal_to(0))

        assert_that(vlan2000.number, equal_to(2000))
        assert_that(vlan2000.name, equal_to("meh"))
        assert_that(len(vlan2000.ips), equal_to(0))

        assert_that(vlan4000.number, equal_to(4000))
        assert_that(vlan4000.name, equal_to(None))
        assert_that(len(vlan4000.ips), equal_to(0))

    def test_add_vlan(self):
        flexmock(self.switch.page_reader).should_receive("do").with_args(self.mocked_ssh_client, "show vlan 1000").and_return([
            "VLAN does not exist."
        ])

        self.mocked_ssh_client.should_receive("do").with_args("vlan database").once().ordered().and_return([])
        self.mocked_ssh_client.should_receive("do").with_args("vlan 1000").once().ordered().and_return([])
        self.mocked_ssh_client.should_receive("do").with_args("exit").once().ordered().and_return([])

        self.switch.add_vlan(1000)

    def test_add_vlan_and_name(self):
        flexmock(self.switch.page_reader).should_receive("do").with_args(self.mocked_ssh_client, "show vlan 1000").and_return([
            "VLAN does not exist."
        ])

        self.mocked_ssh_client.should_receive("do").with_args("vlan database").once().ordered().and_return([])
        self.mocked_ssh_client.should_receive("do").with_args("vlan 1000").once().ordered().and_return([])
        self.mocked_ssh_client.should_receive("do").with_args("vlan name 1000 shizzle").once().ordered().and_return([])
        self.mocked_ssh_client.should_receive("do").with_args("exit").once().ordered().and_return([])

        self.switch.add_vlan(1000, name="shizzle")

    def test_add_vlan_invalid_number(self):
        flexmock(self.switch.page_reader).should_receive("do").with_args(self.mocked_ssh_client, "show vlan 5000").and_return([
            "                                    ^",
            "Value is out of range. The valid range is 1 to 4093."
        ])

        with self.assertRaises(BadVlanNumber) as expect:
            self.switch.add_vlan(5000)

        assert_that(str(expect.exception), equal_to("Vlan number is invalid"))

    def test_add_vlan_and_bad_name(self):
        flexmock(self.switch.page_reader).should_receive("do").with_args(self.mocked_ssh_client, "show vlan 1000").and_return([
            "VLAN does not exist."
        ])

        self.mocked_ssh_client.should_receive("do").with_args("vlan database").once().ordered().and_return([])
        self.mocked_ssh_client.should_receive("do").with_args("vlan 1000").once().ordered().and_return([])
        self.mocked_ssh_client.should_receive("do").with_args("vlan name 1000 Gertr dude").once().ordered().and_return([
            "                                                     ^",
            "% Invalid input detected at '^' marker."
        ])
        self.mocked_ssh_client.should_receive("do").with_args("exit").once().and_return([])

        with self.assertRaises(BadVlanName) as expect:
            self.switch.add_vlan(1000, name="Gertr dude")

        assert_that(str(expect.exception), equal_to("Vlan name is invalid"))

    def test_add_vlan_fails_when_already_exist(self):
        flexmock(self.switch.page_reader).should_receive("do").with_args(self.mocked_ssh_client, "show vlan 1000").and_return([
            "VLAN ID: 40",
            "VLAN Name: deployment",
            "VLAN Type: Static",
            "",
            "Interface   Current   Configured   Tagging",
            "----------  --------  -----------  --------",
            "1/0/1       Exclude   Exclude      Untagged",
            "1/0/2       Include   Include      Tagged",
            "1/0/3       Include   Include      Tagged",
            "1/0/4       Include   Include      Tagged",
            "1/0/5       Include   Include      Tagged"
        ])

        with self.assertRaises(VlanAlreadyExist) as expect:
            self.switch.add_vlan(1000)

        assert_that(str(expect.exception), equal_to("Vlan 1000 already exists"))

    def test_remove_vlan(self):
        self.mocked_ssh_client.should_receive("do").with_args("vlan database").once().ordered().and_return([])
        self.mocked_ssh_client.should_receive("do").with_args("no vlan 1000").once().ordered().and_return([])
        self.mocked_ssh_client.should_receive("do").with_args("exit").once().ordered().and_return([])

        self.switch.remove_vlan(1000)

    def test_remove_unknown_vlan(self):
        self.mocked_ssh_client.should_receive("do").with_args("vlan database").once().ordered().and_return([])
        self.mocked_ssh_client.should_receive("do").with_args("no vlan 1000").once().ordered().and_return([
            "Failed to delete one or more VLAN's. Please refer system log for more information.",
        ])
        self.mocked_ssh_client.should_receive("do").with_args("exit").once().ordered().and_return([])

        with self.assertRaises(UnknownVlan) as expect:
            self.switch.remove_vlan(1000)

        assert_that(str(expect.exception), equal_to("Vlan 1000 not found"))

    def test_set_access_vlan(self):
        with self.configuring_and_committing():
            self.mocked_ssh_client.should_receive("do").with_args("interface 1/0/1").once().ordered().and_return([])
            self.mocked_ssh_client.should_receive("do").with_args("vlan pvid 1000").once().ordered().and_return([])
            self.mocked_ssh_client.should_receive("do").with_args("vlan participation include 1000").once().ordered().and_return([])
            self.mocked_ssh_client.should_receive("do").with_args("no vlan tagging 1000").once().ordered().and_return([])
            self.mocked_ssh_client.should_receive("do").with_args("exit").once().ordered().and_return([])

        self.switch.set_access_vlan("1/0/1", 1000)

    def test_set_access_vlan_invalid_interface(self):
        with self.configuring():
            self.mocked_ssh_client.should_receive("do").with_args("interface 9/9/9").once().ordered().and_return([
                "An invalid interface range has been used for this function. An interface range must be 1000 characters or less."
            ])

        with self.assertRaises(UnknownInterface) as expect:
            self.switch.set_access_vlan("9/9/9", 1000)

        assert_that(str(expect.exception), equal_to("Unknown interface 9/9/9"))

    def test_set_access_vlan_invalid_vlan(self):
        with self.configuring():
            self.mocked_ssh_client.should_receive("do").with_args("interface 1/0/1").once().ordered().and_return([])
            self.mocked_ssh_client.should_receive("do").with_args("vlan pvid 1000").once().ordered().and_return([
                "1/0/1Couldn't configure pvid."
            ])
            self.mocked_ssh_client.should_receive("do").with_args("exit").once().ordered().and_return([])

        with self.assertRaises(UnknownVlan) as expect:
            self.switch.set_access_vlan("1/0/1", 1000)

        assert_that(str(expect.exception), equal_to("Vlan 1000 not found"))

    def test_unset_interface_access_vlan_specific_vlan(self):
        with self.configuring_and_committing():
            self.mocked_ssh_client.should_receive("do").with_args("interface 1/0/1").once().ordered().and_return([])
            self.mocked_ssh_client.should_receive("do").with_args("no vlan pvid 1000").once().ordered().and_return([])
            self.mocked_ssh_client.should_receive("do").with_args("vlan participation auto 1000").once().ordered().and_return([])
            self.mocked_ssh_client.should_receive("do").with_args("exit").once().ordered().and_return([])

        self.switch.unset_interface_access_vlan_specific_vlan("1/0/1", 1000)

    def test_unset_interface_access_vlan_invalid_interface(self):
        with self.configuring():
            self.mocked_ssh_client.should_receive("do").with_args("interface 9/9/9").once().ordered().and_return([
                "An invalid interface range has been used for this function. An interface range must be 1000 characters or less."
            ])

        with self.assertRaises(UnknownInterface) as expect:
            self.switch.unset_interface_access_vlan_specific_vlan("9/9/9", 1000)

        assert_that(str(expect.exception), equal_to("Unknown interface 9/9/9"))

    def test_unset_interface_access_vlan_invalid_vlan(self):
        with self.configuring():
            self.mocked_ssh_client.should_receive("do").with_args("interface 1/0/1").once().ordered().and_return([])
            self.mocked_ssh_client.should_receive("do").with_args("no vlan pvid 1000").once().ordered().and_return([
                "                                                         ^",
                "% Invalid input detected at '^' marker."
            ])
            self.mocked_ssh_client.should_receive("do").with_args("exit").once().ordered().and_return([])

        with self.assertRaises(UnknownVlan) as expect:
            self.switch.unset_interface_access_vlan_specific_vlan("1/0/1", 1000)

        assert_that(str(expect.exception), equal_to("Vlan 1000 not found"))

    @contextmanager
    def configuring_and_committing(self):
        self.mocked_ssh_client.should_receive("do").with_args("configure").once().ordered().and_return([])

        yield

        self.mocked_ssh_client.should_receive("do").with_args("exit").once().ordered().and_return([])

    @contextmanager
    def configuring(self):
        self.mocked_ssh_client.should_receive("do").with_args("configure").once().ordered().and_return([])

        yield

        self.mocked_ssh_client.should_receive("do").with_args("exit").once().ordered().and_return([])
